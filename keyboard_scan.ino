#define KEYBOARD_SCAN_DELAY 6  // TODO get rid of this !!!

boolean scanKeyboard() {

  for (byte i = 0; i < 4; i++) {

    /* These are bit on pads! */
    PORTA = (PORTA & B10111000) | i;
    //delayMicroseconds(KEYBOARD_SCAN_DELAY*5);
    analogRead(TEMPO_POT);
    analogRead(TEMPO_POT);
    analogRead(TEMPO_POT);

    byte pin = PINA >> 3;
    pin = ~pin;

#ifdef GEN_2_PCB
    // blueOn = (blueOn & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));
    // pin >>= 1;
    // yellowOn = (yellowOn & ~(B10000000 >> i)) | ((pin & B00000001) << (7 - i));
    // pin >>= 1;
    // yellowOn = (yellowOn & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));

    blueOn = (blueOn & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));
    yellowOn = (yellowOn & ~(B10000000 >> i)) | ((pin & B00000010) << (6 - i));
    pin >>= 2;
    yellowOn = (yellowOn & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));
#else
    blueOn = (blueOn & ~(B00000001 << i)) | ((pin & B00000001) << i);
    yellowOn = (yellowOn & ~(B00010000 << i)) | ((pin & B00000010) << (i + 3));
    pin >>= 2;
    yellowOn = (yellowOn & ~(B00000001 << i)) | ((pin & B00000001) << i);
#endif

    /* These are bit off pads! */
    PORTA = (PORTA & B10111000) | (i + 4);
    //delayMicroseconds(KEYBOARD_SCAN_DELAY*5);
    analogRead(TEMPO_POT);
    analogRead(TEMPO_POT);
    analogRead(TEMPO_POT);

    pin = PINA >> 3;
    pin = ~pin;

#ifdef GEN_2_PCB
    // blueOff = (blueOff & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));
    // pin >>= 1;
    // yellowOff = (yellowOff & ~(B10000000 >> i)) | ((pin & B00000001) << (7 - i));
    // pin >>= 1;
    // yellowOff = (yellowOff & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));

    blueOff = (blueOff & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));
    yellowOff = (yellowOff & ~(B10000000 >> i)) | ((pin & B00000010) << (6 - i));
    pin >>= 2;
    yellowOff = (yellowOff & ~(B00001000 >> i)) | ((pin & B00000001) << (3 - i));
#else
    blueOff = (blueOff & ~(B00000001 << i)) | ((pin & B00000001) << i);
    yellowOff = (yellowOff & ~(B00010000 << i)) | ((pin & B00000010) << (i + 3));
    pin >>= 2;
    yellowOff = (yellowOff & ~(B00000001 << i)) | ((pin & B00000001) << i);
#endif

  }

  return (blueOn || blueOff || yellowOn || yellowOff);
}
