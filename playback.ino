
/*

  reset pulse
  or
  midi START
  =>
  rewind to 0 and standby

  midi CONTINUE
  or
  run from LOW to HIGH
  or
  start button
  =>
  standby

  midi STOP
  or
  step button
  or
  run from HIGH to LOW
  =>
  stopped

*/


/*
  Rewind sequencer to the start of the current step?
  Next pulse should be downbeat.
*/

void startSequencer() {

  running = true;

  if (settings[SETTINGS_FLAGS] & FLAG_RESET_ON_START) {
    rewindSequencer();
  }

  if (totalPulseCount == 0) {
    sendRealTime(0xfa);  // START
  } else {
    sendRealTime(0xfb);  // CONTINUE
  }

  running = true;
  ci = 1;
  TCNT0 = 0;

  triggers |= trigRunStopMask;
}


void stopSequencer() {

  if (settings[SETTINGS_FLAGS] & FLAG_RESET_ON_STOP) {
    rewindSequencer();
  }

  running = false;

  triggers &= ~trigRunStopMask;
  //triggers &= ~trigResetMask;
  //triggers &= ~trigStepClkMask;
  //triggers &= ~trigPulseClkMask;

  sendRealTime(0xFC);
}


void rewindSequencer() {
  settings[SETTINGS_STEP_COUNTER] = 0;
  pulseCounter = 0;
  step = 0;
  triggers |= trigResetMask;
  totalPulseCount = 0;
}


void pulse() {

  /*
    ?????
    When in standby mode, the playback will start either when:
      1. in master_mode trig pulse has been played and trigger down.
      2. in slave_mode, either pulse or step pulse is received.
      3. in slave_mode, midi beat clock is received.

    In these cases, stopped is set to false, which will force
    initiating of the first downbeat (without pulsecounter++).
  */

  totalPulseCount++;

  if (pulseCounter > settings[SETTINGS_PULSES_PER_STEP] * 2) {
    if (running) {
      nextStep();
    }
    pulseCounter = 1;
  }

  triggers |= trigPulseClkMask;
  triggers &= ~trigResetMask;

  sendRealTime(0xf8);
}


void resetPulse() {

  triggers |= trigPulseClkMask;

  if (settings[SETTINGS_MIDI_OUT] & FLAG_MIDI_BEAT_CLK_ENABLE) {
    MIDI.sendRealTime(0xF8);
  }
}


void nextStep() {

  pulseCounter = 1;
  int s = settings[SETTINGS_STEP_COUNTER] + settings[SETTINGS_STEP_INCREMENT];
  s %= settings[SETTINGS_SEQUENCE_LAST_STEP] + 1;

  if ((settings[SETTINGS_FLAGS] & FLAG_ONE_SHOT) && running) {

    if (settings[SETTINGS_FLAGS] & FLAG_LAST_STEP) {

      if (s == 0) {
        stopSequencer();
        return;
      }

    } else {

      if (settings[SETTINGS_STEP_COUNTER] == settings[SETTINGS_SEQUENCE_LAST_STEP]) {
        stopSequencer();
        return;
      }
    }
  }

  settings[SETTINGS_STEP_COUNTER] = s;
  step = getStep();
  resetStep();
  updateDivider();
}


void resetStep() {
  triggers |= trigStepClkMask;
}


void sendRealTime(byte b) {
  if (settings[SETTINGS_MIDI_OUT] & FLAG_MIDI_BEAT_CLK_ENABLE) {
    MIDI.sendRealTime(b);
  }
}