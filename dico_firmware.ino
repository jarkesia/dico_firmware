#include <SPI.h>
#include <EEPROMex.h>
#include <MIDI.h>
#include <avr/pgmspace.h>
#include <ATmegaADC.h>

/*
    Firmware for DICO-DIR.
    Copyright (C) 2020-2024  Jari Suominen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



/*
  Firmware for DICO-DIR board atmega644.
*/

/*
    FUSES: HF: 0xDE LF: 0xFF EF: 0xFD
    avrdude -c avrispmkII -p m644p -P /dev/ttyACM0 -U lfuse:w:0xff:m -U hfuse:w:0xde:m -U efuse:w:0xfd:m -F

    EESAVE ON:
    FUSES: HF: 0xD6 LF: 0xFF EF: 0xFD
    avrdude -c avrispmkII -p m644p -P /dev/ttyACM0 -U lfuse:w:0xff:m -U hfuse:w:0xD6:m -U efuse:w:0xfd:m -F

    ARDUINO IDE:
    Sanguino / ATmega644PA (16Mhz)
*/

/* Original User Manual:
  (C)   0000            1011      1   110
        1010      (A)   1100      2   010
        0011            0101      3   100
  (Eb)  0100            1111      4   000
        1110      sk     111           11
        0111      k      011           01
  (Gb)  1000      s      101           10
        0001      p      001           00
*/

/*

        0000 000 = C7 / 2093Hz

        0000 101 = C2
        0000 011 = C1
        0000 111 = C0 / 16Hz

  (G-1)   0010

  (Bb-1)  0110

  (Cx)    0000
  (Db)    1010
  (Dx)    0011
  (Eb)    0100
  (Ex)    1110
  (Fx)    0111
  (Gb)    1000
  (Gx)    0001
  (Ab)    1011
  (Ax)    1100
  (Bb)    0101
  (Bx)    1111

  (Db+1)  1001

  (E2+1)  1101

*/

/*
  Remember false == 0, true != false !
*/

/*
  Settings:
  0000 - Step counter / timeline position
  0001 - Last step of the sequence (0-255)
  0010 - Pulses per step (4th = 24, 8th = 12, 16 = 6, 8th triplet = 16, and so on... Pulses per quarter note (PPQ) is always 24!
  0011 - Step increment (0-255)
  0100 - Tempo potentiometer reading (Read only)
  0101 - Input/output jack status indication (Read only)
  0110 - Sequence counter
  0111 - Sequence count
  1000 - CV outputs #1 (MSB) and #2 (LSB)
  1001 - CV outputs #3 (MSB) and #4 (LSB)
  1010 - MIDI IN ( > 15 == OMNI)
  1011 - MIDI OUT
  1100 - CV inputs #1 (MSB) and #2 (LSB)
  1101 - Random flags/settings
  1110 - Divider B ratios (on=MSB, off=LSB)
  1111 - Divider A ratios (on=MSB, off=LSB)

  Note that the setting bit code is in MSB while on the PCB it is LSB.

*/

MIDI_CREATE_DEFAULT_INSTANCE();

inline void latchOutputs() __attribute__((always_inline));
inline void resetPulse() __attribute__((always_inline));
inline void resetStep() __attribute__((always_inline));
inline void nextStep() __attribute__((always_inline));
inline void updateTrigOutMasks() __attribute__((always_inline));
inline void pulse() __attribute__((always_inline));
inline byte getStep() __attribute__((always_inline));
inline void updatePWM() __attribute__((always_inline));

/* V2.1 */

/* These two are for university museum DICO */
//#define LEGACY_MODE_SWITCH
//#define EXHIBITION_MODE
//#define LEGACY_FUTURE_EXPANSION_BITS  // When either bit 0 or 1 is enabled, both are lit temporarily

#define GEN_2_PCB  // Only HY unit is gen 2, others are gen 3. No more gen 2 will ever be built.

#ifdef GEN_2_PCB
#define LEGACY_FUTURE_EXPANSION_BITS
#define EXHIBITION_MODE
#define LEGACY_MODE_SWITCH
#endif

#define DIVIDER_GATE  // Emulates divider network gating of the original hardware. Maybe move to a user configurable settings?

#define MOSI 5
#define MISO 6
#define SCK 7
#define SS PB4

/* ANALOG INPUT */
#define TEMPO_POT A6

//#define PPS_8TH 6
//#define PPQ_ROLAND 24
//#define PPQ_KORG 48
#define PPQ 24

byte pulseCounter = 0;
boolean running = false;
unsigned long int totalPulseCount = 0;

#define PWM OCR2B

#define MEMORY_MAX_SIZE 256
byte memory[MEMORY_MAX_SIZE][2];  // One full bank would be 510 bytes. RAM in total is 4096 bytes.

#define MEMORY_CHANNEL B00000100
#define MEMORY_Z B00000010
#define MEMORY_Y B00000001

byte oscillator = 0;

#define MODE_MASTER 1  // Normal mode with buttons enabled
#define MODE_SLAVE 2
#define MODE_LEGACY_PLAY 3
#define MODE_LEGACY_EDIT 4
byte mode = MODE_MASTER;

boolean settingEdit = false;
byte settingPointer = 0;
/* We don't bother to swap the order of bytes here, that's why the weird order. */
#define SETTINGS_STEP_COUNTER 0        // 0000
#define SETTINGS_SEQUENCE_LAST_STEP 1  // 0001
#define SETTINGS_PULSES_PER_STEP 2     // 0010
#define SETTINGS_STEP_INCREMENT 3      // 0011
#define SETTINGS_TEMPO 4               // 0100
#define SETTINGS_INDICATOR 5           // 0101
#define SETTINGS_TEMPO_RANGE 5         // 0101
#define SETTINGS_SEQUENCE_COUNTER 6    // 0110
#define SETTINGS_LAST_SEQUENCE 7       // 0111

#define SETTINGS_CV_OUT_1_2 8    // 1000
#define SETTINGS_CV_OUT_3_4 9    // 1001
#define SETTINGS_MIDI_IN 10      // 1010
#define SETTINGS_MIDI_OUT 11     // 1011
#define SETTINGS_TRIG_IN_1_2 12  // 1100
#define SETTINGS_FLAGS 13        // 1101
#define SETTINGS_B_DIVIDER 14    // 1110
#define SETTINGS_A_DIVIDER 15    // 1111

#define SETTINGS_POINTER 16

#define MSB(b) (b >> 4)
#define LSB(b) (b & B00001111)

// FLAGS

#define FLAG_RESET_ON_START 1
#define FLAG_RESET_ON_STOP 2
#define FLAG_ONE_SHOT 4
#define FLAG_LAST_STEP 8
#define FLAG_ENABLE_GATE 16
#define FLAG_ENABLE_CHANNEL 32
#define FLAG_GATE_OPEN_ON_STOP 64

#define TEMPO_CLOCK_HZ 0
#define TEMPO_CLOCK_BPM 1

#define FLAG_MIDI_THRU_ENABLE 16
#define FLAG_MIDI_OMNI_ON 16
#define FLAG_MIDI_BEAT_CLK_ENABLE 32
#define FLAG_MIDI_NOTE_DATA_ENABLE 64


// DIGITAL INPUTS:
#define TRIG_IN_1_MASK B10000000     // PORTD
#define TRIG_IN_2_MASK B00010000     // PORTD
#define BUTTON_STEP_MASK B00000100   // PORTB
#define BUTTON_START_MASK B00001000  // PORTB

// DIGITAL OUTPUTS:

byte triggers = 0;  // 3rd 74595
#ifdef GEN_2_PCB
#define TRIG_INDICATOR_MASK B00100000  // PORTC
#define TRIG_OUT_1_MASK B00000001      // SHIFT REGISTER
#define TRIG_OUT_2_MASK B00000010      // SHIFT REGISTER
#define TRIG_OUT_3_MASK B00000100      // SHIFT REGISTER
#define HARDWARE_GATE B00001000        // PORTC
#define HARDWARE_CHANNEL B00010000     // PORTC
#define TRIG_GATE_1_MASK B01000000     // NOT USED IN GEN_2 BUT WE NEED THE DATA
#define TRIG_GATE_2_MASK B10000000     // NOT USED IN GEN_2 BUT WE NEED THE DATA
#else
#define TRIG_INDICATOR_MASK B00000001  // ALL TRIGGERS THROUGH SHIFT REGISTER
#define TRIG_OUT_1_MASK B00000010
#define TRIG_OUT_2_MASK B00000100
#define TRIG_OUT_3_MASK B00001000
#define TRIG_GATE_1_MASK B00010000
#define TRIG_GATE_2_MASK B00100000
#endif



byte trigGateMask = 0;
byte trigChannelMask = 0;
byte trigStepClkMask = 0;
byte trigPulseClkMask = 0;
byte trigResetMask = 0;
byte trigRunStopMask = 0;
byte trigDebugMask = 0;
byte trigIn1Mask = 0;
byte trigIn2Mask = 0;

#define TRIG_OUT_NOT_USED 0
#define TRIG_OUT_GATE 1
#define TRIG_OUT_ 2
#define TRIG_OUT_DEBUG 3
#define TRIG_OUT_CHANNEL 4
#define TRIG_OUT_STEP_CLK 5
#define TRIG_OUT_PULSE_CLK 6
#define TRIG_OUT_RESET 7
#define TRIG_OUT_RUN_STOP 8
#define TRIG_OUT_IN_1 12
#define TRIG_OUT_IN_2 13

#define CV_OUT_STEP 1
#define CV_OUT_NOTE 2
#define CV_OUT_NOTE_CH_1 3
#define CV_OUT_NOTE_2_OCT 4
#define CV_OUT_CHANNEL_2 5
#define CV_OUT_BYTE 6
#define CV_OUT_POT 7
#define CV_OUT_PITCH 8

#define TRIG_IN_STEP_CLK 1
#define TRIG_IN_PULSE_CLK 2
#define TRIG_IN_NEXT_SEQUENCE 3
#define TRIG_IN_RESET 4
#define TRIG_IN_GATE_MODULATOR 5
#define TRIG_IN_RUN_STOP 8
#define TRIG_IN_START_STOP 9
#define TRIG_IN_VIBRATO_DOWN 14
#define TRIG_IN_VIBRATO_UP 15

int vibrato = 0;
boolean gateOpen = true;

#define SETTINGS_COUNT 17
byte settings[SETTINGS_COUNT] = {
  0,
  B00001011,
  B00001100,
  B00000001,
  0,
  B01010000,
  0,
  0,
  TRIG_OUT_GATE,
  TRIG_OUT_PULSE_CLK,
  B00000001,  // MIDI IN
  B00000000,  // MIDI OUT
  TRIG_OUT_CHANNEL,
  B11110000,  // RANDOM FLAGS
  B01000011,  // DIVIDER B RATIO
  B00100011,  // DIVIDER A RATIO
  B00000000   // Last setting
};

unsigned int divider = 2;
byte note = 0;

int settingsEEPROM;
//int memoryEEPROM;

const int maxAllowedWrites = 80;  // ???
const int memBase = 350;          // ???

/* LEDs are MSB but keyboard is LSB, that's why! */
const byte reverse4[] = {
  B00000000,
  B00001000,
  B00000100,
  B00001100,
  B00000010,
  B00001010,
  B00000110,
  B00001110,
  B00000001,
  B00001001,
  B00000101,
  B00001101,
  B00000011,
  B00001011,
  B00000111,
  B00001111
};


/* Scale for midi note conversion.
  Note that in order to access very lowest notes of the instrument,
  you would need to use different combinations, for instance G with bit B, not A.

  TODO: This array  is obsolete after custom divider ratios where added!

*/
const byte scale[]{
  B00000000,
  B00000101,
  B00001100,
  B00000010,
  B00000111,
  B00001110,
  B00000001,
  B00001000,
  B00001101,
  B00000011,
  B00001010,
  B00001111
};

/* Oscillator select offset for midi note / cv out conversion */
#define FUNDAMENTAL 134
byte oscillatorNote[]{
  FUNDAMENTAL, 6 + FUNDAMENTAL, 3 + FUNDAMENTAL, 9 + FUNDAMENTAL
};

byte harmonicSeries[]{
  0, 0, 12, 19, 24, 28, 31, 34, 36, 38, 40, 42, 43, 44, 46, 47
};

boolean startRequest = false;
boolean stopRequest = false;
boolean standbyRequest = false;
boolean stepRequest = false;
boolean rewindRequest = false;

const char signMessage[] PROGMEM = { "I AM PREDATOR,  UNSEEN COMBATANT. CREATED BY THE UNITED STATES DEPART" };

#ifdef EXHIBITION_MODE
boolean exhibitionStandby = false;
unsigned long exhibitionStandbyCounter = 0;
#endif



volatile byte blueOn;
volatile byte blueOff;
volatile byte yellowOn;
volatile byte yellowOff;

byte step;

boolean forcePotRead = false;

ATmegaADC adc = ATmegaADC(6);


void setup() {

  SPI.setClockDivider(SPI_CLOCK_DIV2);
  SPI.begin();

  // EEPROMex:
  EEPROM.setMemPool(memBase, 2048);
  EEPROM.setMaxAllowedWrites(maxAllowedWrites);
  delay(100);

  int settingsNAN = EEPROM.getAddress(sizeof(byte));
  settingsNAN = 0xFF;  // force factory settings
  settingsEEPROM = EEPROM.getAddress(sizeof(byte) * SETTINGS_COUNT);

  if (EEPROM.readByte(settingsNAN) != 0xFF) {  // If EEPROM has ever written or not.
    EEPROM.readBlock<byte>(settingsEEPROM, settings, SETTINGS_COUNT);
  } else {
    EEPROM.updateByte(settingsNAN, 0);
    EEPROM.updateBlock<byte>(settingsEEPROM, settings, SETTINGS_COUNT);
  }

  TCCR0A = B00000011;
  TCCR0B = B00001101;  // Prescaler 1024, will be overridden soon in the DCO
  TIMSK0 = B00000010;

  /* Fast-PWM on every pin, pwm frequency ~62500Hz */
  TCCR2A = B10100001;
  TCCR2B = B00000001;

  TCCR1A = B01000000;  // Toggle on compare match
  TCCR1B = B00001110;  // Clock on falling edge
  //TIMSK1 = B00000010;

  /* All unused pins set to INPUT and pulled high */
  DDRA = B00000111;
  PORTA = B10111000;
  DDRB = B10110000;
  PORTB = B01001101;
  DDRC = B00111111;
  PORTC = B11000111;
  DDRD = B01100010;
  PORTD = B10011100;

  // Trig in interrupts
  PCICR = B00001010;  // PORTD and PORTB scan enabled
  PCMSK1 = BUTTON_STEP_MASK | BUTTON_START_MASK;
  PCMSK3 = TRIG_IN_1_MASK | TRIG_IN_2_MASK;

  resetMemory(false);

  PWM = 0;

  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.setHandleClock(midiClock);
  MIDI.setHandleStart(midiStart);
  MIDI.setHandleContinue(midiContinue);
  MIDI.setHandleStop(midiStop);

  MIDI.setHandleNoteOn(midiNoteOn);
  //MIDI.setHandleNoteOff(recordEventStop);
  //MIDI.setHandleControlChange(midiCC);
  //MIDI.turnThruOff();

  rewindSequencer();
  stopSequencer();

  updateTrigOutMasks();

  while (!adc.available()) {};
  settings[SETTINGS_TEMPO] = adc.read();

  step = getStep();
  updateDivider();
  cli();
  latchOutputs();
  sei();
}


void loop() {

#ifdef EXHIBITION_MODE
  if (!exhibitionStandby && (exhibitionStandbyCounter > 22153)) {  // roughly minute
    exhibitionStandby = true;
    TCCR1A = B00000000;  // Normal pin operation
    startSequencer();
    updateGate();
    latchOutputs();
  } else {
    exhibitionStandbyCounter++;
  }
#endif

  while (MIDI.read()) {}

  if (settingEdit) {
    if (scanKeyboard()) {

      byte b = settings[SETTINGS_POINTER];
      byte y = settings[settings[SETTINGS_POINTER]];

      settings[SETTINGS_POINTER] |= blueOn;
      settings[SETTINGS_POINTER] &= ~blueOff;
      settings[settings[SETTINGS_POINTER]] |= yellowOn;
      settings[settings[SETTINGS_POINTER]] &= ~yellowOff;

      if ((b != settings[SETTINGS_POINTER]) || (y != settings[settings[SETTINGS_POINTER]])) {

        if ((settings[SETTINGS_POINTER] == SETTINGS_CV_OUT_1_2) || (settings[SETTINGS_POINTER] == SETTINGS_CV_OUT_3_4) || (settings[SETTINGS_POINTER] == SETTINGS_INDICATOR)) {
          updateTrigOutMasks();
        }
        if ((LSB(settings[SETTINGS_TRIG_IN_1_2]) < TRIG_IN_VIBRATO_DOWN) && (MSB(settings[SETTINGS_TRIG_IN_1_2]) < TRIG_IN_VIBRATO_DOWN)) {
          vibrato = 0;
        }
        if ((LSB(settings[SETTINGS_TRIG_IN_1_2]) < TRIG_IN_GATE_MODULATOR) && (MSB(settings[SETTINGS_TRIG_IN_1_2]) < TRIG_IN_GATE_MODULATOR)) {
          gateOpen = true;
        }
        if ((settings[SETTINGS_POINTER] == SETTINGS_B_DIVIDER) || (settings[SETTINGS_POINTER] == SETTINGS_A_DIVIDER)) {
          updateDivider();
        }
        if (settings[SETTINGS_POINTER] == SETTINGS_TEMPO_RANGE) {
          if (LSB(settings[SETTINGS_TEMPO_RANGE]) & TEMPO_CLOCK_BPM) {
            dcoUpdate(settings[SETTINGS_TEMPO] + 37, true);
          } else {
            dcoUpdate(settings[SETTINGS_TEMPO], false);
          }
        }

        cli();
        latchOutputs();
        sei();
      }
    }
  } else {
    if (scanKeyboard()) {

#ifdef EXHIBITION_MODE
      exhibitionStandbyOff();
#endif

      byte b = memory[step][0];
      byte y = memory[step][1];

      memory[step][0] |= blueOn;
      memory[step][0] &= ~blueOff;
      memory[step][1] |= yellowOn;
      memory[step][1] &= ~yellowOff;

      if ((b != memory[step][0]) || (y != memory[step][1])) {

        updateDivider();
        updateGate();
        cli();
        latchOutputs();
        sei();
      }
    }
  }

  /*
    This gives as 8 bit value with 0% jitter, using non-blocking read.
  */
  if (adc.available()) {
    if (adc.valueChanged()) {
      settings[SETTINGS_TEMPO] = adc.read();
      //updateInternalClockTempo();
      if (LSB(settings[SETTINGS_TEMPO_RANGE]) & TEMPO_CLOCK_BPM) {
        dcoUpdate(settings[SETTINGS_TEMPO] + 37, true);
      } else {
#ifdef EXHIBITION_MODE
        dcoUpdate(max(settings[SETTINGS_TEMPO], 1), false);
#else
        dcoUpdate(settings[SETTINGS_TEMPO], false);
#endif
      }
#ifdef EXHIBITION_MODE
      exhibitionStandbyOff();
#endif
      updatePWM();
    }
  }

  // Mode switch
#ifdef LEGACY_MODE_SWITCH
  if (PINC >> 7) {
    if (mode != MODE_LEGACY_PLAY) {
      mode = MODE_LEGACY_PLAY;
      startSequencer();
      exhibitionStandbyOff();
    }
  } else {
    if (mode != MODE_MASTER) {
      mode = MODE_MASTER;
      stopSequencer();
      exhibitionStandbyOff();
    }
  }
#else

  byte m = PINC >> 6;

  switch (m) {

    case MODE_MASTER:
    case MODE_SLAVE:

      if (settingEdit) {
        // TODO at some point change writes only happen during power down...
        EEPROM.updateBlock<byte>(settingsEEPROM, settings, SETTINGS_COUNT);
        settingEdit = false;
        latchOutputs();
      }

      if (mode != m) {
        mode = m;
        triggers &= ~trigPulseClkMask;  //?
        latchOutputs();
      }

      break;
    default:
      if (!settingEdit) {
        settingEdit = true;
        latchOutputs();
      }
      break;
  }
#endif
}


byte getStep() {
  return (settings[SETTINGS_STEP_COUNTER] + settings[SETTINGS_SEQUENCE_COUNTER] * (MEMORY_MAX_SIZE / (settings[SETTINGS_LAST_SEQUENCE] + 1))) % MEMORY_MAX_SIZE;
}


void updateDivider() {

  if (!settingEdit) {
    memory[step][0] |= blueOn;
    memory[step][0] &= ~blueOff;
    memory[step][1] |= yellowOn;
    memory[step][1] &= ~yellowOff;
  }

  note = oscillatorNote[(memory[step][0] & B00001100) >> 2];

  unsigned int d = 1;

  /* Divider A (5th) */
  if (memory[step][0] & B00000001) {
    d *= MSB(settings[SETTINGS_A_DIVIDER]);
    note -= harmonicSeries[MSB(settings[SETTINGS_A_DIVIDER])];
  } else {
    d *= LSB(settings[SETTINGS_A_DIVIDER]);
    note -= harmonicSeries[LSB(settings[SETTINGS_A_DIVIDER])];
  }

  /* Divider B (4th) */
  if (memory[step][0] & B00000010) {
    d *= MSB(settings[SETTINGS_B_DIVIDER]);
    note -= harmonicSeries[MSB(settings[SETTINGS_B_DIVIDER])];
  } else {
    d *= LSB(settings[SETTINGS_B_DIVIDER]);
    note -= harmonicSeries[LSB(settings[SETTINGS_B_DIVIDER])];
  }

  /* If divider is 0 we will naturally not divide at all! */
  if (d == 0) {
    TCCR1B = B00001000;
    TCNT1 = 0;
  } else {
    TCCR1B = B00001110;
  }

  d = d << reverse4[((memory[step][1] & B11100000) >> 4)];  // Octave select.
  d -= 1;                                                   // Divider 0 already would divide frequency down by half.
  note -= 12 * reverse4[((memory[step][1] & B11100000) >> 4)];

  //if (!(memory[step][1] & MEMORY_CHANNEL) | !(memory[step][1] & MEMORY_Z)) { // Old feature idea, probably will be removed totally.
  divider = d;  // Will be updated in COMPA interrupt.
  //}

  oscillator = (B00010000 << ((memory[step][0] >> 2) & B00000011));
}


#ifdef EXHIBITION_MODE
void exhibitionStandbyOff() {
  exhibitionStandbyCounter = 0;
  if (exhibitionStandby) {
    TCCR1A = B01000000;
    exhibitionStandby = false;
  }
}
#endif


void updatePWM() {
  /* PWM CV out */
  /* TODO, move somewhere else, no need to keep updating constantly */
  switch (LSB(settings[SETTINGS_CV_OUT_3_4])) {
    case CV_OUT_STEP:
      PWM = settings[SETTINGS_STEP_COUNTER];
      break;
    case CV_OUT_NOTE:
      PWM = (note % 12) * 2;
      break;
    case CV_OUT_NOTE_2_OCT:
      PWM = (note % 24) * 2;
      break;
    case CV_OUT_POT:
      PWM = settings[SETTINGS_TEMPO];
      break;
    case CV_OUT_PITCH:
      PWM = note * 2;
      break;
  }
}