void updateGate() {

  /*
      Gate

      Internally original DICO would always run (kind of) 8 ticks per step.
      Based on how DA/DB bits are set, possible lengths of note on a step are:
      1, 2, 4 or full 8 ticks.

      DIR-DICO allows different amount of ticks per step. This will distort
      the ratios of different lengths of notes somewhat. For instance default
      12 ticks (one step being 8th) would result to length such as:
      1, 3, 6 and 12.

      Note that pulsegenerator is running in double rate.

  */

  byte b = trigGateMask | TRIG_GATE_1_MASK | TRIG_GATE_2_MASK;
  triggers &= ~b;

  if (running || !(settings[SETTINGS_FLAGS] & FLAG_GATE_OPEN_ON_STOP)) {

    switch (memory[step][1] & B00011000) {
      case B00000000:
        if (pulseCounter > (settings[SETTINGS_PULSES_PER_STEP] / 4)) {
          b = 0;
        }
        break;
      case B00010000:
        if (pulseCounter > (settings[SETTINGS_PULSES_PER_STEP] / 2)) {
          b = 0;
        }
        break;
      case B00001000:
        if (pulseCounter > (settings[SETTINGS_PULSES_PER_STEP])) {
          b = 0;
        }
        break;
    }
  }

  if (pulseCounter > (settings[SETTINGS_PULSES_PER_STEP])) {
    triggers &= ~trigStepClkMask;
  }

  if (!gateOpen) {  // extr modulator/tremolo
    b = 0;          //???
  }

  if (!(settings[SETTINGS_FLAGS] & FLAG_ENABLE_GATE)) {
    b |= TRIG_GATE_1_MASK | TRIG_GATE_2_MASK;
  }

  /* Output channel select */
  if (!(memory[step][1] & MEMORY_CHANNEL)) {
    /* Channel #2 ON, bit CH OFF */
    triggers &= ~trigChannelMask;
    if (settings[SETTINGS_FLAGS] & FLAG_ENABLE_CHANNEL) {
      b &= ~TRIG_GATE_2_MASK;
    }
  } else {
    /* Channel #1 */
    triggers |= trigChannelMask;
    if (settings[SETTINGS_FLAGS] & FLAG_ENABLE_CHANNEL) {
      b &= ~TRIG_GATE_1_MASK;
    }
  }

  triggers |= b;
}


void updateTrigOutMasks() {

#ifdef GEN_2_PCB
  trigChannelMask = HARDWARE_CHANNEL;
  trigGateMask = HARDWARE_GATE;
  trigStepClkMask = TRIG_INDICATOR_MASK;
#else
  trigChannelMask = 0;
  trigGateMask = 0;
  trigStepClkMask = 0;
#endif
  trigDebugMask = 0;
  trigPulseClkMask = 0;
  trigResetMask = 0;
  trigRunStopMask = 0;
  trigIn1Mask = 0;
  trigIn2Mask = 0;

  selectTrigMask(MSB(settings[SETTINGS_CV_OUT_1_2]), TRIG_OUT_1_MASK);
  selectTrigMask(LSB(settings[SETTINGS_CV_OUT_1_2]), TRIG_OUT_2_MASK);
  selectTrigMask(MSB(settings[SETTINGS_CV_OUT_3_4]), TRIG_OUT_3_MASK);
  selectTrigMask(MSB(settings[SETTINGS_INDICATOR]), TRIG_INDICATOR_MASK);
}


void selectTrigMask(byte trigger, byte mask) {
  switch (trigger) {
    case TRIG_OUT_GATE:
      trigGateMask |= mask;
      break;
    case TRIG_OUT_DEBUG:
      trigDebugMask |= mask;
      break;
    case TRIG_OUT_CHANNEL:
      trigChannelMask |= mask;
      break;
    case TRIG_OUT_STEP_CLK:
      trigStepClkMask |= mask;
      break;
    case TRIG_OUT_PULSE_CLK:
      trigPulseClkMask |= mask;
      break;
    case TRIG_OUT_RESET:
      trigResetMask |= mask;
      break;
    case TRIG_OUT_RUN_STOP:
      trigRunStopMask |= mask;
      break;
    case TRIG_OUT_IN_1:
      trigIn1Mask |= mask;
      break;
    case TRIG_OUT_IN_2:
      trigIn2Mask |= mask;
      break;
  }
}
