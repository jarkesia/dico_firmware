/* 

  As 16-bit timer is used in dividing down the main oscillator, we need to use
  8-bit timer for tempo oscillator. As the tempo oscillator of DICO needs to enter
  the audible range, we need to do it this way to make sure there is absolutely no
  jitter (typical sequencer might easily have 1ms jitter or more!).

  We are using Timer0 for this!

  Stock range of DICO is from 1 step per second to 200 steps per second.
  Internally DICO tempo oscillator will tick eight times per step.

  DCO will have two main range modes, one where input is simply Hz
  and another one where the input is in BPM.

  For BPM we simply use 24 pulses per quarternote rule. Note that user might
  have set the pulsecount per step to be whatever from 0 to 255, which will
  effectively change the signature of a single step, but not actual bpm.

  Roland TR-909 tempos are from 37BPM to 290BPM.
  
 */

// These are read by interrupt routine.
// MHO reading and writting these don't need to be volatile.
byte dcoIterations = 1; // How many times we count the byte buffer through before a tick.
byte dcoBytes = 255;    // All bytes but the last byte will be counting to this.
byte dcoLastByte = 255;

void dcoUpdate(unsigned int hz, boolean bpm) {

  static byte pulses = 12; // Not sure whether to keep 12 or 8 or PPQ. For now will just assume step is 8th note.

  static unsigned long prescalers[] =
  {
    // To speed things up, recalculating there based on fixed pulse rate...
    16000000 / pulses / 1024,
    16000000 / pulses / 256,
    16000000 / pulses / 64,
    16000000 / pulses / 8,
    16000000 / pulses
  };
  
  static unsigned long prescalersBPM[] =
  {
    (16000000.0 / PPQ / 1024) * 60,
    (16000000.0 / PPQ / 256) * 60,
    (16000000.0 / PPQ / 64) * 60,
    (16000000.0 / PPQ / 8) * 60,
    (16000000.0 / PPQ) * 60
  };

  static byte tccr[] = {
    B00001101,
    B00001100,
    B00001011,
    B00001010,
    B00001001,
  };

  unsigned int dcoTop = 0; // Technically we should probably check that < (65025 + 1)
  byte p;

  if (hz == 0) {
    // This should stop the sequencer DCO without actually stopping sequencer.
    dcoIterations = 255;
  } else {
    p = 0;
    do {
      if (bpm) {
        dcoTop = prescalersBPM[p] / hz;
      } else {
        dcoTop = prescalers[p] / hz; 
      }
      if (dcoTop > 510) break; // For accuracy we would prefer at least three byte calculation. Could be more or less.
      p++;
    } while (p < 5);

    TCCR0B = tccr[p];

    if (dcoTop % 255 == 0) {
      dcoIterations = dcoTop / 255;
      dcoBytes = 255;
    } else {
      dcoIterations = dcoTop / 255 + 1;
      dcoBytes = dcoTop / dcoIterations + 1; // Note that in case of 1 byte counting, only dcoLastByte is used!
    }
    dcoLastByte = dcoTop - (dcoBytes * (dcoIterations - 1));

  }

}

