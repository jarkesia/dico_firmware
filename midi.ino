void midiStart() {

  if (running) {
    return;
  }

  if ((settings[SETTINGS_MIDI_IN] & FLAG_MIDI_BEAT_CLK_ENABLE) && mode == MODE_SLAVE) {
    rewindSequencer();
    startSequencer();
    
    cli();
    updateGate();
    latchOutputs();
    cli();
  }
}


void midiContinue() {
  if ((settings[SETTINGS_MIDI_IN] & FLAG_MIDI_BEAT_CLK_ENABLE) && mode == MODE_SLAVE) {
    if (!running) {
      
    }
  }
}


void midiClock() {
  if ((settings[SETTINGS_MIDI_IN] & FLAG_MIDI_BEAT_CLK_ENABLE) && (mode == MODE_SLAVE)) {
    pulse();
    cli();
    updateGate();
    latchOutputs();
    cli();
  }
}


void midiStop() {

  if (!running) {
    return;
  }

  if ((settings[SETTINGS_MIDI_IN] & FLAG_MIDI_BEAT_CLK_ENABLE) && mode == MODE_SLAVE) {
    cli();
    stopSequencer();
    updateGate();
    latchOutputs();
    sei();
  }
}


void midiNoteOn(byte channel, byte pitch, byte velocity) {
  /* We should actually implement some kind of mono keyboard scheme that would manage multiple keys at the same time and held keys also. */
  if ((settings[SETTINGS_MIDI_IN] & FLAG_MIDI_NOTE_DATA_ENABLE)) {
    if ((settings[SETTINGS_MIDI_IN] & FLAG_MIDI_OMNI_ON) || (settings[SETTINGS_MIDI_IN] & B00001111) == (channel - 1)) {

      memory[step][1] = (((pitch / 12) - 1 << 4) & B11100000) | (memory[settings[SETTINGS_STEP_COUNTER]][1] & B00011111);
      memory[step][0] = scale[pitch % 12];
      
      cli();
      latchOutputs();
      sei();

    }
  }
}
