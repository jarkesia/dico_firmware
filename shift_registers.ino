/*
  This will flush all precalculated triggers, oscillator selects and
  the main divider settings to the outputs at the same moment.
*/
void latchOutputs() {

#ifdef EXHIBITION_MODE
  if (exhibitionStandby) {
#ifdef GEN_2_PCB
    triggers &= ~HARDWARE_GATE;
#else
    triggers &= ~(TRIG_GATE_1_MASK | TRIG_GATE_2_MASK);
#endif
  }
#endif

  PORTB &= B11101111;
  SPI.transfer(triggers);

#ifdef GEN_2_PCB
  byte a = reverse4[memory[step][1] & B00001111];
  a |= oscillator;
#ifdef LEGACY_FUTURE_EXPANSION_BITS
  if (yellowOn & B00000011) {
    a |= B00001100;
  } else {
    a &= B11110011;
  }
#endif

  byte b = reverse4[memory[step][0] & B00001111] | (reverse4[(memory[step][1] & B11110000) >> 4]) << 4;
  SPI.transfer(~a);
  SPI.transfer(~b);

#else

  if (settingEdit) {
    SPI.transfer(~(settings[SETTINGS_POINTER] | oscillator));
    SPI.transfer(~settings[settings[SETTINGS_POINTER]]);
  } else {
    SPI.transfer(~(memory[step][0] | oscillator));

#ifdef LEGACY_FUTURE_EXPANSION_BITS
    if (yellowOn & B00000011) {
      SPI.transfer(~(memory[step][1] | B00000011));
    } else {
      SPI.transfer(~(memory[step][1] & B11111100));
    }
#else
    SPI.transfer(~memory[step][1]);
#endif
  }
#endif

#ifdef DIVIDER_GATE
  if (triggers & (TRIG_GATE_1_MASK | TRIG_GATE_2_MASK)) {
    DDRB &= B11111101;
  } else {
    DDRB |= B00000010;
  }
#endif

  OCR1A = divider + vibrato;
  if (TCNT1 > OCR1A) /* Counter has escaped! */
    TCNT1 %= OCR1A;

  PORTB |= B00010000;  // !REMEMBER! THAT OUTPUTS WILL BE LATCHED IN THIS MOMENT!

#ifdef GEN_2_PCB
  PORTC = ~triggers | B11000000;
#endif

  updatePWM();
}
