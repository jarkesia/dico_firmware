void resetMemory(boolean legacy) {

  for (int i = 0; i < MEMORY_MAX_SIZE; i++) {
    memory[i][0] = random(255) & B00001111;
    memory[i][1] = random(255) & B11111000;
  }

  if (legacy) {

    // Original DICO 2007? random sequence after power on transcripted from wav file

    //    memory[0][0] =  B00001111; memory[0][1] =  B00010100;
    //    memory[1][0] =  B00001001; memory[1][1] =  B00000100;
    //    memory[2][0] =  B00001011; memory[2][1] =  B00000100;
    //    memory[3][0] =  B00001100; memory[3][1] =  B11101100; // ~27.5Hz = A0
    //    memory[4][0] =  B00000111; memory[4][1] =  B00101100;
    //    memory[5][0] =  B00001110; memory[5][1] =  B01011100;
    //    memory[6][0] =  B00000110; memory[6][1] =  B11000100; //
    //    memory[7][0] =  B00000000; memory[7][1] =  B10000100;
    //    memory[8][0] =  B00000010; memory[8][1] =  B01100100; // ~24.7Hz = G0/G#0
    //    memory[9][0] =  B00001010; memory[9][1] =  B10101100; // ~70Hz = Db2
    //    memory[10][0] = B00000111; memory[10][1] = B00000100; // ~2844Hz = F7
    //    memory[11][0] = B00001000; memory[11][1] = B10100100; // ~93.5Hz = F#2

    // Found original transcript of the 2007 sequence from my memo:

    memory[0][0] = B00001000;
    memory[0][1] = B01100100;
    memory[1][0] = B00000001;
    memory[1][1] = B00000000;
    memory[2][0] = B00000000;
    memory[2][1] = B01000000;
    memory[3][0] = B00000001;
    memory[3][1] = B00001000;
    memory[4][0] = B00001101;
    memory[4][1] = B10000000;
    memory[5][0] = B00001100;
    memory[5][1] = B00000000;
    memory[6][0] = B00000000;
    memory[6][1] = B11100100;
    memory[7][0] = B00000001;
    memory[7][1] = B00110000;
    memory[8][0] = B00000111;
    memory[8][1] = B01011000;
    memory[9][0] = B00000001;
    memory[9][1] = B11000000;
    memory[10][0] = B00001101;
    memory[10][1] = B01100000;
    memory[11][0] = B00000000;
    memory[11][1] = B11100100;

  } else {

    // memory[0][0] =  B00000000;  memory[0][1] =  B00011000;
    // memory[1][0] =  B00000001;  memory[1][1] =  B10011100;
    // memory[2][0] =  B00000011;  memory[2][1] =  B00011000;
    // memory[3][0] =  B00000100;  memory[3][1] =  B10011100;
    // memory[4][0] =  B00000101;  memory[4][1] =  B00011000;
    // memory[5][0] =  B00000110;  memory[5][1] =  B10011100;
    // memory[6][0] =  B00000111;  memory[6][1] =  B00011000;
    // memory[7][0] =  B00001000;  memory[7][1] =  B10011100;
    // memory[8][0] =  B00001001;  memory[8][1] =  B00011000;
    // memory[9][0] =  B00001010;  memory[9][1] =  B10011100;
    // memory[10][0] = B00001011;  memory[10][1] = B00011000;
    // memory[11][0] = B00001100;  memory[11][1] = B10011100;
  }

#ifdef EXHIBITION_MODE
  for (int i = 0; i < MEMORY_MAX_SIZE; i++) {
    memory[i][1] &= B11111000;
  }
#endif
}
