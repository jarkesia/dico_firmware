inline void processTrigIn(byte settings, boolean rising) __attribute__((always_inline));

//ISR(TIMER1_COMPA_vect) {
//  /* This is done in interupt so that the timer don't escape when divider is set smaller than what it used to be.
//     DISABLED ON MK2.1 as not consistent with original hardware. Function left here instead there will be some other
//     for it later. */
//  OCR1A = random(255);
//
//}


byte ci = 1;  // counter iteration, [1,byte_count]
ISR(TIMER0_COMPA_vect) {

  /* Have to do more complete rewrite as falling edge timing
     is not really fixed and chances depending of the byte count. */

#ifdef LEGACY_MODE_SWITCH
  if ((mode == MODE_MASTER) || (mode == MODE_LEGACY_PLAY)) {
#else
  if (mode == MODE_MASTER) {
#endif
    if (running) {
      if (ci == (dcoIterations / 2)) {
        /* Sloppy falling */
        if (pulseCounter == 0) {
          ci = 1;
          triggers &= ~trigResetMask;
          pulse();
        } else {
          triggers &= ~trigPulseClkMask;
          ci++;
        }
        pulseCounter++;
      } else if (ci > (dcoIterations - 1)) {
        /* Tight rising */
        pulseCounter++;
        pulse();
        ci = 1;
      } else {
        ci++;
      }

    } else {
      triggers &= ~trigResetMask;
      triggers &= ~trigRunStopMask;
      triggers &= ~trigPulseClkMask;
    }
    updateGate();
    latchOutputs();
  } else {
    ci = 1;
  }

  if (ci == dcoIterations) {
    OCR0A = dcoLastByte;
  } else {
    OCR0A = dcoBytes;
  }
}


byte portbHistory = PINB;
ISR(PCINT1_vect) {

  byte pinb = ~PINB & (BUTTON_STEP_MASK | BUTTON_START_MASK); /* TODO I guess using masks here is pointless? */
  byte changedbits = pinb ^ portbHistory;
  portbHistory = pinb;

#ifdef LEGACY_MODE_SWITCH
  exhibitionStandbyOff();
  if (mode == MODE_LEGACY_PLAY) {
    return;
  }
#endif

  if (((BUTTON_STEP_MASK | BUTTON_START_MASK) & portbHistory) == (BUTTON_STEP_MASK | BUTTON_START_MASK)) {
    rewindSequencer();
    if (running) {
      startSequencer();
    }
    updateGate();
    updateDivider();
    latchOutputs();
  } else {

    if (changedbits & BUTTON_START_MASK & pinb) {
      /* Start button pressed */
      startSequencer();
      updateGate();
      updateDivider();
      latchOutputs();
    }

    if (changedbits & BUTTON_STEP_MASK & pinb) {
      if (!running) {
        nextStep();
        updateGate();
        updateDivider();
        latchOutputs();
      } else {
        stopSequencer();
      }
    }
  }

  delayMicroseconds(500); /* A bit nasty during an interrupt, but works for now... */
}


byte portdHistory = PIND;

ISR(PCINT3_vect) {
  byte pind = PIND;
  byte changedbits;
  changedbits = pind ^ portdHistory;
  portdHistory = pind;

  if (changedbits & TRIG_IN_1_MASK) {
    if (TRIG_IN_1_MASK & ~portdHistory) {  // rising edge
      processTrigIn(MSB(settings[SETTINGS_TRIG_IN_1_2]), true);
      triggers |= trigIn1Mask;
#ifdef EXHIBITION_MODE
      resetMemory(true);
      exhibitionStandbyOff();
      updateGate();
      updateDivider();
      latchOutputs();
#endif
    } else {  // falling edge
      processTrigIn(MSB(settings[SETTINGS_TRIG_IN_1_2]), false);
      triggers &= ~trigIn1Mask;
#ifdef EXHIBITION_MODE
      resetMemory(false);
      exhibitionStandbyOff();
      updateGate();
      updateDivider();
      latchOutputs();
#endif
    }
  }

  if (changedbits & TRIG_IN_2_MASK) {
    if (TRIG_IN_2_MASK & ~portdHistory) {  // rising edge
      triggers |= trigIn2Mask;
      processTrigIn(LSB(settings[SETTINGS_TRIG_IN_1_2]), true);
    } else {  // falling edge
      processTrigIn(LSB(settings[SETTINGS_TRIG_IN_1_2]), false);
      triggers &= ~trigIn2Mask;
    }
  }
  latchOutputs();
}


void processTrigIn(byte tsettings, boolean rising) {

  if (rising) {
    switch (tsettings) {
      case TRIG_IN_STEP_CLK:
        if (mode == MODE_SLAVE) {
          nextStep();
          updateGate();
        }
        break;
      case TRIG_IN_PULSE_CLK:
        if (mode == MODE_SLAVE) {
          pulseCounter++;
          pulse();
          updateGate();
        }
        break;
      case TRIG_IN_RESET:
        //standbyRequest = true;
        rewindSequencer();
        break;
      case TRIG_IN_RUN_STOP:
        startSequencer();
        break;
      case TRIG_IN_START_STOP:
        if (running) {
          //stopRequest = true;
          stopSequencer();
        } else {
          startSequencer();
        }
        break;
      case TRIG_IN_GATE_MODULATOR:
        gateOpen = true;
        break;
      case TRIG_IN_NEXT_SEQUENCE:
        settings[SETTINGS_SEQUENCE_COUNTER] = (settings[SETTINGS_SEQUENCE_COUNTER] + 1) % (settings[SETTINGS_LAST_SEQUENCE] + 1);
        break;
      case TRIG_IN_VIBRATO_DOWN:
        vibrato = 1;
        break;
      case TRIG_IN_VIBRATO_UP:
        vibrato = -1;
        break;
      default:
        break;
    }
  } else {
    switch (tsettings) {
      case TRIG_IN_PULSE_CLK:
        if (mode == MODE_SLAVE) {
          pulseCounter++;
          updateGate();
        }
        break;
      case TRIG_IN_RUN_STOP:
        //stopRequest = true;
        stopSequencer();
        break;
      case TRIG_IN_GATE_MODULATOR:
        gateOpen = false;
        break;
      case TRIG_IN_VIBRATO_DOWN:
      case TRIG_IN_VIBRATO_UP:
        vibrato = 0;
        break;
      default:
        break;
    }
  }
}
